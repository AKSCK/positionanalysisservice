package main

import (
	"fmt"
	"time"

	"./position"
	"go.uber.org/dig"
)

func main() {
	dig := dig.New()
	dig.Invoke(NewSettings)
	fmt.Println("Hello World")
	err, service := position.NewPositionService("localhost:9092")
	if err != nil {
		fmt.Println("Some NewPositionService error occured", err)
		return
	}
	go service.Start()

	fmt.Println("Start Kafka")
	time.Sleep(10 * time.Minute)
}
