package main

type Settings struct {
	BootstrapServers string
}

func NewSettings(mapConfig map[string]interface{}) *Settings {
	return &Settings{
		BootstrapServers: mapConfig["BootstrapServers"].(string),
	}
}

func BootstrapServers(s Settings) string {
	return s.BootstrapServers
}
