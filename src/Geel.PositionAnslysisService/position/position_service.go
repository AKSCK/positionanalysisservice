package position

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/segmentio/kafka-go"
)

type PositionService struct {
	reader     *kafka.Reader
	readerConf kafka.ReaderConfig
	writer     *kafka.Writer
	writerConf kafka.WriterConfig
	connectUrl string
}

func NewPositionService(connectUrl string) (error, PositionService) {
	var posService PositionService
	posService.connectUrl = connectUrl
	return nil, posService
}
func (ps PositionService) Start() {

	ps.readerConf = kafka.ReaderConfig{
		Brokers:  []string{ps.connectUrl},
		Topic:    "positon-Debug",
		GroupID:  "g1",
		MaxBytes: 10,
	}

	ps.reader = kafka.NewReader(ps.readerConf)

	ps.writerConf = kafka.WriterConfig{
		Brokers: []string{ps.connectUrl},
		Topic:   "positon-More-Debug",
	}

	ps.writer = kafka.NewWriter(ps.writerConf)

	for {
		const count int = 1
		var posList []position
		for i := 0; i < count; i++ {
			m, err := ps.reader.ReadMessage(context.Background())
			if err != nil {
				fmt.Println("Some ReadMessage error occured", err)
				continue
			}
			fmt.Println("Message is :", string(m.Value))
			var pos position
			errUn := json.Unmarshal(m.Value, &pos)
			if errUn != nil {
				fmt.Println("Some Unmarshal error occured", err)
				continue
			}
			posList = append(posList, pos)
		}
		ps.ProcessPosition(posList)
	}
}

func (ps PositionService) ProcessPosition(posList []position) {
	err, posClient := NewPositionClient()
	if err != nil {
		fmt.Println("Some NewPositionClient error occured", err)
		return
	}
	usersPosition := map[string]position{}
	for _, pos := range posList {
		if val, ok := usersPosition[pos.Name]; ok {
			usersPosition = ps.CheckPosition(pos.Y-val.Y, pos, usersPosition)
		} else {
			err, posBd := posClient.GetPosition(pos.Name)
			if err != nil {
				fmt.Println("Some GetPosition error occured", err)
				errInsert := posClient.InsertPosition(pos)
				if errInsert != nil {
					fmt.Println("Some InsertPosition error occured", errInsert)
				} else {
					usersPosition[pos.Name] = pos
				}
			} else {
				fmt.Println("Got Position ", posBd)
				usersPosition = ps.CheckPosition(pos.Y-posBd.Y, pos, usersPosition)
			}
		}
	}

	for _, user := range usersPosition {
		err = posClient.UpdatePosition(user)
		if err != nil {
			fmt.Println("Some UpdatePosition error occured", err)
		}
	}
}

func (ps PositionService) CheckPosition(difference float64, pos position, usersPosition map[string]position) map[string]position {
	if difference < 6 {
		usersPosition[pos.Name] = pos
		fmt.Println("Update position ", pos.Name)
	} else {
		fmt.Println("New position more old position for ", difference)
		posJson, err := json.Marshal(pos)
		if err != nil {
			fmt.Println("New position Marshal error ", err)
		}
		ps.writer.WriteMessages(context.Background(), kafka.Message{
			Value: posJson,
		})
	}
	return usersPosition
}
