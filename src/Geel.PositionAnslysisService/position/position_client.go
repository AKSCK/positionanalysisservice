package position

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type PositionClient struct {
	client *mongo.Client
}

func NewPositionClient() (error, PositionClient) {
	var posClient PositionClient
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017/"))
	if err != nil {
		return err, posClient
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err != nil {
		return err, posClient
	}
	err = client.Connect(ctx)
	if err != nil {
		return err, posClient
	}
	posClient.client = client
	return err, posClient
}

func (posClient PositionClient) InsertPosition(pos position) error {

	collection := posClient.client.Database("Geel").Collection("Position")

	insertResult, err := collection.InsertOne(context.TODO(), pos)

	fmt.Println("Inserted position with ID:", insertResult.InsertedID)
	return err
}
func (posClient PositionClient) UpdatePosition(pos position) error {

	collection := posClient.client.Database("Geel").Collection("Position")
	filter := bson.D{{"name", pos.Name}}
	update := bson.M{
		"$set": pos,
	}
	_, err := collection.UpdateOne(context.TODO(), filter, update)

	fmt.Println("Updated position")
	return err
}
func (posClient PositionClient) GetPosition(name string) (error, position) {

	collection := posClient.client.Database("Geel").Collection("Position")
	filter := bson.D{{"name", name}}
	var pos position
	err := collection.FindOne(context.TODO(), filter).Decode(&pos)
	return err, pos
}
