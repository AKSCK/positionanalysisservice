package position

type IPositionSettings interface {
	BootstrapServers() string
}
