package position

type position struct {
	X    float64
	Y    float64
	Name string
}
